module Read_Write_m
  use Settings_m
  use Types_m

  implicit none


contains

  subroutine readSquareMatrixData( matrix, filename, size, binary )
    real( 8 ), dimension(:, :), allocatable, intent( inout ) :: matrix
    character( * ), intent( in ) :: filename
    integer( 8 ), intent( in ) :: size
    logical, intent( in ) :: binary
    integer( 8 ) :: io_error

    if ( binary ) then
      open( unit = 50, file = path//filename, action = 'read', &
       form = 'unformatted', status = 'old', iostat = io_error, & 
       access = 'direct', recl = size **2 * 8 )
    else
      open( unit = 50, file = path//filename, action = 'read', iostat = io_error )
    end if

    if ( io_error .eq. 0 ) then
      allocate( matrix( size, size ) )

      if ( binary ) then
        read( 50, rec = 1 ) matrix
      else 
        read( 50, * ) matrix
      end if
    else
      write(*,*) 'Beim Öffenen der Datei ist ein Fehler Nr.', &
                       io_error,' aufgetreten'
    end if

    close( unit = 50, iostat = io_error )

  end subroutine readSquareMatrixData


end module Read_Write_m
