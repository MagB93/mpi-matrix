
add_executable( read_benchmarks read_benchmark.f90 )

add_dependencies( read_benchmarks Basic )
target_link_libraries( read_benchmarks Basic )

set_property(TARGET read_benchmarks PROPERTY LINKER_LANGUAGE Fortran)
set_target_properties( read_benchmarks
                       PROPERTIES
                       ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib"
                       LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib"
                       RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/bin"
                       )

install(TARGETS read_benchmarks
        RUNTIME DESTINATION bin
        LIBRARY DESTINATION lib
        ARCHIVE DESTINATION lib/static)
