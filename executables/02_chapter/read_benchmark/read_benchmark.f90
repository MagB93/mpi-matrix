program read_benchmark
  use Read_Write_m

  integer( 8 ), parameter :: matrix_10 = 10
  integer( 8 ), parameter :: matrix_1000 = 1000
  character( * ), parameter :: file_name_A_10_human = "A_10x10.data"
  character( * ), parameter :: file_name_A_10_bin = "A_10x10.bin"
  character( * ), parameter :: file_name_A_1000_human = "A_1000x1000.data"
  character( * ), parameter :: file_name_A_1000_bin = "A_1000x1000.bin"
  real( 8 ), dimension( :, : ), allocatable :: matrix_10x10_human
  real( 8 ), dimension( :, : ), allocatable :: matrix_1000x1000_human
  real( 8 ), dimension( :, : ), allocatable :: matrix_10x10_bin
  real( 8 ), dimension( :, : ), allocatable :: matrix_1000x1000_bin
  real( 8 ) :: start_10_human, start_1000_human
  real( 8 ) :: finish_10_human, finish_1000_human
  real( 8 ) :: start_10_bin, start_1000_bin
  real( 8 ) :: finish_10_bin, finish_1000_bin

  ! First the small matrices
  call cpu_time( start_10_human )
  call readSquareMatrixData( matrix_10x10_human, file_name_A_10_human, matrix_10, .false. )
  call cpu_time( finish_10_human )

  write( *,* ) file_name_A_10_human
  write( *,* ) "Took: ", finish_10_human - start_10_human, " seconds "

  call cpu_time( start_10_bin )
  call readSquareMatrixData( matrix_10x10_bin, file_name_A_10_bin, matrix_10, .true. )
  call cpu_time( finish_10_bin )

  write( *,* ) file_name_A_10_bin
  write( *,* ) "Took: ", finish_10_bin - start_10_bin, " seconds "

  ! Now the larger ones
  call cpu_time( start_1000_human )
  call readSquareMatrixData( matrix_1000x1000_human, file_name_A_1000_human, matrix_1000, .false. )
  call cpu_time( finish_1000_human )

  write( *,* ) file_name_A_1000_human
  write( *,* ) "Took: ", finish_1000_human - start_1000_human,  " seconds "

  call cpu_time( start_1000_bin )
  call readSquareMatrixData( matrix_1000x1000_bin, file_name_A_1000_bin, matrix_1000, .true. )
  call cpu_time( finish_1000_bin )

  write( *,* ) file_name_A_1000_bin
  write( *,* ) "Took: ",  finish_1000_bin - start_1000_bin, " seconds "


end program read_benchmark
